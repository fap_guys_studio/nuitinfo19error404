# nuitInfo19Error404

Projet jeu page 404 de la nuit de l'info 2019
Pur JS bien crade, avec du html pas beau et des libs importés à la mano.

## Installation :
- Cloner ou telecharger le répo
- Double cliquer sur index.html
- Voila c'est tout

## Utilisation
- Swipe à droite, à gauche ou en haut pour jouer.

Ecran large uniquement (ordinateur et tablette)

### Idées
Adapter le gameplay de reign pour les étudiants en galere.

#### Scores :
4 barre de score à maximiser de [0, 100] sans qu'aucune ne tombe à 0 !

- vie sociale
- état de santé
- argent
- réussite scolaire

#### Gameplay :
Une action ext proposée et 3 choix sont disponibles :
- oui
- non
- Aléatoire

#### Exemple :
Action : "Sortir boire un verre"
Choix :
- oui : +2 vie sociale, -1 argent
- non : +1 reussite
- aléatoire : [-5, +5]

#### Evenement aléatoires
De temps en temps et aléatoirement des evenements viennet apporter des bonus !

#### Basé sur Reign

https://store.steampowered.com/app/474750/Reigns/?l=french
![image du jeu reign](https://steamcdn-a.akamaihd.net/steam/apps/474750/ss_ee14f2c6acbb1021b24757b01949a8767b17b9d4.600x338.jpg?t=1568612606)
