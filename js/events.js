const events = {
  "events": [{
    "text": "Mamie vient apporter son aide !",
    "money": 10
  }, {
    "text": "Le gouvernement augmente les APL !",
    "money": 1
  }, {
    "text": "Concert gratuit du CROUS !",
    "social": 5
  }, {
    "text": "La ville offre des paniers de legumes aux etudiants",
    "sante": 10
  }, {
    "text": "Le ru s'est trompé en votre faveur !",
    "money": 5
  },{
    "text": "Une petite soirée de jeu de société",
    "social": 10
  },{
    "text": "Maman a fait plein de tupperware",
    "sante": 5,
    "money": 5
  },{
    "text": "Un peu de sous trouvé au fond dun jean",
    "money": 5
  },{
    "text": "Un prof qui vous veut du bien vous a donné un de ses cours",
    "scolaire": 10
  }]
}.events
