window.onload = setup
window.onresize = setLayout
let card, canSwipe, dead = false, round = 0
let values = {scolaire: 0, sante: 0, social: 0, argent: 0}

function setLayout() {
  document.body.style.height = window.innerHeight + 'px'
}

function setup() {
  $('#modal').modal()
  setLayout()
  setSwipe()
  setEvents()
  addStatus(50, 50, 50, 50)
  newLoop()
}

function newLoop() {
  setIdle()
  randomEvents()
  getNewCard()
  applyCard()
  round++
}

function randomEvents() {
  if (round === 0)
    return
  if (Math.random() > 0.8) {
    const index = Math.floor(Math.random() * events.length)
    const event = events[index]
    document.getElementById('event-text').innerText = event.text
    $('#event').toast({delay: 3000}).toast('show')
    addStatus(event.scolaire, event.money, event.sante, event.social)
  }
}

function setSwipe() {
  document.getElementById('de').addEventListener('click', swipeUp)
  document.getElementById('non').addEventListener('click', swipeLeft)
  document.getElementById('oui').addEventListener('click', swipeRight)
  setSwipeOn(document)
  const plateau = document.getElementById('carte').childNodes[0]
  setSwipeOn(plateau)
}

function setSwipeOn(element) {

  const swipe = new Hammer(element)
  swipe.get('swipe').set({direction: Hammer.DIRECTION_ALL})
  swipe.on('swipeleft', swipeLeft)
  swipe.on('swipeup', swipeUp)
  swipe.on('swiperight', swipeRight)
}

function addStatus(scolaire = 0, argent = 0, sante = 0, social = 0) {
  setStatusValue(scolaire, 'scolaire')
  setStatusValue(argent, 'argent')
  setStatusValue(sante, 'sante')
  setStatusValue(social, 'social')
}

function getNewCard() {
  const index = Math.floor(Math.random() * cards.length)
  card = cards[index]
}

function setEvents() {
  const card = document.getElementById('carte')
  card.addEventListener('animationend', (event) => {
    if (dead) {
      card.style.display = 'none'
      setTimeout(() => {
        window.location.reload()
      }, 2000)
    }
    card.classList.remove('goRight', 'goLeft', 'goUp', 'death')
    switch (event.animationName) {
      case 'goRightKF':
        applyModifiers('right')
        break
      case 'goLeftKF':
        applyModifiers('left')
        break
      case 'goUpKF':
        applyModifiers('random')
        break
    }
  })
}

function swipeLeft() {
  if (!canSwipe)
    return
  setNoIdle()
  animate('goLeft')
}

function swipeRight() {
  if (!canSwipe)
    return
  setNoIdle()
  animate('goRight')
}

function swipeUp() {
  if (!canSwipe)
    return
  setNoIdle()
  animate('goUp')
}

function animate(animation) {
  canSwipe = false
  const card = document.getElementById('carte')
  card.classList.add(animation)
}

function applyCard() {
  document.getElementById('text').innerText = card.value
}

function applyModifiers(selector) {
  let sante, social, scolaire, argent
  if (selector === 'random') {
    argent = computeRandomValue(card.option[selector].money)
    sante = computeRandomValue(card.option[selector].health)
    social = computeRandomValue(card.option[selector].social)
    scolaire = computeRandomValue(card.option[selector].academic)
  } else {
    argent = card.option[selector].money
    sante = card.option[selector].health
    social = card.option[selector].social
    scolaire = card.option[selector].academic
  }


  addStatus(scolaire, argent, sante, social)
  newLoop()
}

function computeRandomValue(modifier) {
  const max = modifier.max
  const min = modifier.min
  return Math.floor(Math.random() * (max - min)) + min
}

function setStatusValue(value, element) {
  const progress = document.getElementById(element).childNodes[1]
  const left = progress.childNodes[1].childNodes[1]
  const right = progress.childNodes[3].childNodes[1]

  value += values[element]
  values[element] = value

  if (value > 0) {
    if (value <= 50) {
      left.style.transform = 'rotate(0deg)'
      right.style.transform = 'rotate(' + percentageToDegrees(value) + 'deg)'
    } else {
      value = value > 100 ? 100 : value
      right.style.transform = 'rotate(180deg)'
      left.style.transform = 'rotate(' + percentageToDegrees(value - 50) + 'deg)'
    }
  } else {
    left.style.transform = 'rotate(0deg)'
    right.style.transform = 'rotate(0deg)'
    death()
  }
}

function death() {
  canSwipe = false
  dead = true
  animate('death')
}

function setIdle() {
  canSwipe = true
  const card = document.getElementById('carte')
  card.classList.add('idle')
}

function setNoIdle() {
  canSwipe = false
  const card = document.getElementById('carte')
  card.classList.remove('idle')
}

function percentageToDegrees(percentage) {
  return percentage / 100 * 360
}
